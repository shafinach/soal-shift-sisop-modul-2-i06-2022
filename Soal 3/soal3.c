#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<wait.h>
#include<string.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>


void movejpg(){

    int status;
    pid_t child_id = fork();

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0) {
            char *argv[] = {"find", "/home/fina/soal3/animal", "-iname", "*air*", "-exec", "cp", "{}", "/home/fina/soal3/air/", ";", NULL};
            execv("/usr/bin/find", argv);
    } 
    else {
        while(wait(&status) > 0);
        pid_t child_id2 = fork();
        
        if(child_id2 < 0) 
            exit(EXIT_FAILURE);
            
        if(child_id2 == 0) {
            char *argv[] = {"find", "/home/fina/soal3/animal", "-iname", "*darat*", "-exec", "cp", "{}", "/home/fina/soal3/darat/", ";", NULL};
            execv("/usr/bin/find", argv);
        } 
        else {
            while(wait(&status) > 0);
            pid_t child_id3 = fork();

            if(child_id3 < 0) 
                exit(EXIT_FAILURE);

            if(child_id3 == 0) {
                char *argv[] = {"rm", "-r", "/home/fina/soal3/animal", NULL};
                execv("/usr/bin/rm", argv);
            } else {
                while(wait(&status) > 0);
            }
        }
    }

}

void unzip(){

    int status;
    pid_t child_id3;
    child_id3 = fork();

    if(child_id3 < 0){
		exit(EXIT_FAILURE);
	}
    if(child_id3 == 0){
		char *argv3 [] = {"unzip", "animal.zip", "-d", "/home/fina/soal3/", NULL};
        execv("/usr/bin/unzip", argv3);
	}
    else {
        while(wait(&status) > 0);
        movejpg();
    } 

}

void removeBird(){
    
    pid_t child_id;
    child_id = fork();

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0) {
        char *argv[] = {"find", "/home/fina/soal3/darat", "-iname", "*bird*", "-exec", "rm", "{}", ";", NULL}; 
        execv("/usr/bin/find", argv);
    }

}

int main(){
	char folder_air[1001] = {"/home/fina/soal3/air"};
	char folder_darat[1001] = {"/home/fina/soal3/darat"};
	pid_t child_id;
    pid_t child_id2;
	child_id = fork();
    child_id2 = fork();

    int status;
	
	if(child_id < 0){
		exit(EXIT_FAILURE);
	}
	if(child_id == 0){
		char *argv [] = {"mkdir", "-p", folder_darat, NULL};
        execv("/usr/bin/mkdir", argv);

	}
    else{
        while ((wait(&status)) > 0);
        if(child_id2 == 0){
        sleep(3);
        char *argv2 [] = {"mkdir", "-p", folder_air, NULL};
        execv("/usr/bin/mkdir", argv2);
        }
    }

    while(wait(&status) > 0);
    unzip();

    while(wait(&status) > 0);
    removeBird();
	
	return 0;
}
