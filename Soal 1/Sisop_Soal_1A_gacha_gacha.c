#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
 
int main()
{
 pid_t id_child1 = fork();
 
 if( id_child1 == 0)
 {
     char *mkdir[] = {"mkdir", "-p", "gacha_gacha", NULL};
     execv("/bin/mkdir", mkdir);
 }
 
 pid_t id_child2 = fork();
 
 if( id_child2 == 0 )
 {
     char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "genshin_characters.zip", NULL};
     execv("/bin/wget", args);
 }
 
 pid_t id_child3 = fork();
 
 if( id_child3 == 0 )
 {
     char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "genshin_weapon.zip", NULL};
     execv("/bin/wget", args);
 }
 
 sleep(10);
 
 pid_t id_child4 = fork();
 
 if( id_child4 == 0 )
 {
    execlp("unzip","unzip","-q","/root/gacha_gacha/genshin_characters.zip",NULL);
 }
 
 pid_t id_child5 = fork();
 
 if( id_child5 == 0 )
 {
    execlp("unzip","unzip","-q","/root/gacha_gacha/genshin_weapon.zip",NULL);
 }
}