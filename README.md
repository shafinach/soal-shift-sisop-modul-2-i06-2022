# soal-shift-sisop-modul-2-I06-2022

I06 Sisop Group 2022

- Yusuf Faiz Shalahudin (5025201119)
- Shafina Chaerunisa (5025201129)
- Bagus Nugraha (5025201162)

## Soal 1

a. Diminta oleh soal untuk membuat sebuah directory lalu mendownload file dari https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view dan https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view yang berisikan genshin character dan genshin weapons, lalu masukan hasil download kedalam directory gacha_gacha.


```
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
 
int main()
{
 pid_t id_child1 = fork();
 
 if( id_child1 == 0)
 {
     char *mkdir[] = {"mkdir", "-p", "gacha_gacha", NULL};
     execv("/bin/mkdir", mkdir);
 }
 
 pid_t id_child2 = fork();
 
 if( id_child2 == 0 )
 {
     char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "genshin_characters.zip", NULL};
     execv("/bin/wget", args);
 }
 
 pid_t id_child3 = fork();
 
 if( id_child3 == 0 )
 {
     char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "genshin_weapon.zip", NULL};
     execv("/bin/wget", args);
 }
 
 sleep(10);
 
 pid_t id_child4 = fork();
 
 if( id_child4 == 0 )
 {
    execlp("unzip","unzip","-q","/root/gacha_gacha/genshin_characters.zip",NULL);
 }
 
 pid_t id_child5 = fork();
 
 if( id_child5 == 0 )
 {
    execlp("unzip","unzip","-q","/root/gacha_gacha/genshin_weapon.zip",NULL);
 }
}


```




## Soal 2

a.	Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

```
void createshift(){ 
    
    int status;
    pid_t pid1 ;
    
    pid1 = fork();
    
    
    if(pid1 == 0) {
    char *argv[]={"mkdir","shift2",NULL};
    execv("/bin/mkdir",argv);
    }
    

}


void unzipfile(){ 
     int stats;
    pid_t Child_id = fork();
    
        
    if (Child_id < 0) {
            exit(EXIT_FAILURE); 
        }

    if (Child_id == 0) {
        char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "/home/sugab/shift2/drakor", NULL};
            execv("/bin/unzip", argv);
        } 
    
    
}


```

This piece of code is to create the task for create folder shift and unzip the zip from drakor.zip in home with only png file to drakor directory in shift 2 directory. The code is basically initialize pid first and then fork the pid that have been created.After that, we put the command in an argument with argv like we use command on terminal and then call function execv
## Soal 3

a.Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.


```
    char folder_air[1001] = {"/home/fina/soal3/air"};
	char folder_darat[1001] = {"/home/fina/soal3/darat"};
```

Combining fork and execv, so we can run task one by one. If child_id == 0, it'll create a new directory name 'darat'
```
if(child_id < 0){
		exit(EXIT_FAILURE);
	}
	if(child_id == 0){
		char *argv [] = {"mkdir", "-p", folder_darat, NULL};
        execv("/usr/bin/mkdir", argv);

	}
```

Else, child_id2 == 0, also create a new directory name 'air' but we wait for 3 seconds after folder 'darat' made
```
while ((wait(&status)) > 0);
    if(child_id2 == 0){
    sleep(3);       //wait for 3 seconds
    char *argv2 [] = {"mkdir", "-p", folder_air, NULL};
    execv("/usr/bin/mkdir", argv2);
```

b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

```
 if(child_id3 == 0){
		char *argv3 [] = {"unzip", "animal.zip", "-d", "/home/fina/soal3/", NULL};
        execv("/usr/bin/unzip", argv3);
	}
    else {
        while(wait(&status) > 0);
        movejpg();
    } 
```

Through new variable of char, it'll unzip 'animal.zip' file into soal3 folder

c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

```
if(child_id == 0) {
    char *argv[] = {"find", "/home/fina/soal3/animal", "-iname", "*air*", "-exec", "cp", "{}", "/home/fina/soal3/air/", ";", NULL};
    execv("/usr/bin/find", argv);
    } 
```
If child_id == 0, it'll move the picture file that has 'air' name on it into folder 'air'.


```
if(child_id2 == 0) {
    char *argv[] = {"find", "/home/fina/soal3/animal", "-iname", "*darat*", "-exec", "cp", "{}", "/home/fina/soal3/darat/", ";", NULL};
    execv("/usr/bin/find", argv);
        } 
```
Also the same with child_id2, it'll move the picture file that has 'darat' name on it into folder 'darat'.

After that if there's no 'air' or 'darat' name on it, it'll be deleted immediately.

```
char *argv[] = {"rm", "-r", "/home/fina/soal3/animal", NULL};
execv("/usr/bin/rm", argv);
```

d. Conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

```
 if(child_id == 0) {
    char *argv[] = {"find", "/home/fina/soal3/darat", "-iname", "*bird*", "-exec", "rm", "{}", ";", NULL}; 
    execv("/usr/bin/find", argv);
    }
```
The code is also the same as before, but we just have to change into 'bird' to find the file name that has 'bird' on it to be deleted.
